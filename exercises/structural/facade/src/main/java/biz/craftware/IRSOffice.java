package biz.craftware;

import biz.craftware.domain.farm.Animal;

public interface IRSOffice {

	void applyVaccsine(Animal cattle);
}
