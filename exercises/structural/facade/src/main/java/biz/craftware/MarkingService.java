package biz.craftware;

import biz.craftware.domain.farm.Animal;

public interface MarkingService {

	void markCow(Animal cattle);
}
