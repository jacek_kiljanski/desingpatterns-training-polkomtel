package biz.craftware;

import biz.craftware.domain.farm.Animal;

public interface Veterinary {

	void applyVaccsine(Animal cattle);
}
